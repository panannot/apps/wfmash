# wfmash Singularity container
### Package wfmash Version 0.16.0
wfmash is an aligner for pangenomes based on sparse homology mapping and wavefront inception.

wfmash uses a variant of MashMap to find large-scale sequence homologies. It then obtains base-level alignments using WFA, via the wflign hierarchical wavefront alignment algorithm.
wfmash is designed to make whole genome alignment easy. On a modest compute node, whole genome alignments of gigabase-scale genomes should take minutes to hours, depending on sequence divergence. It can handle high sequence divergence, with average nucleotide identity between input sequences as low as 70%.
wfmash is the key algorithm in pggb (the PanGenome Graph Builder), where it is applied to make an all-to-all alignment of input genomes that defines the base structure of the pangenome graph. It can scale to support the all-to-all alignment of hundreds of human genomes.
Additional tools:
samtools v1.19
ibcftools v1.20 (bgzip)

Homepage:

https://github.com/waveygang/wfmash

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
wfmash Version: 0.16.0<br>
Singularity container based on the recipe: Singularity.wfmash_v0.16.0.def

Local build:
```
sudo singularity build wfmash_v0.16.0.sif Singularity.wfmash_v0.16.0.def
```

Get image help:
```
singularity run-help wfmash_v0.16.0.sif
```

Default runscript: wfmash<br>
Usage:
```
./wfmash_v0.16.0.sif --help
```
or:
```
singularity exec wfmash_v0.16.0.sif wfmash --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull wfmash_v0.16.0.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/wfmash/wfmash:latest

```

